package number22;

public class NumberTwentyTwo {

	// Lilin yang pertama meleleh
	
	public static void main(String[] args) {
		// INPUT
		int[] fibonacci = {1, 1, 2, 3, 5, 8, 13};
		int[] candle = {3, 3, 9, 6, 7, 8, 23};
		int[] tempCandle = candle.clone();
		int meltCandle = 0;
		int n = 0;
		
		
		// PROSES
		boolean isMelt = false;
		while(!isMelt) {
			for(int i=0; i<tempCandle.length; i++) {
				tempCandle[i] -= fibonacci[i];

				if(tempCandle[i]<=0) {
					isMelt = true;
					meltCandle = candle[i];
					n = i + 1;
					break;
				}
			}
		}
		
		
		// OUTPUT
		System.out.println("Lilin yang paling pertama habis meleleh adalah lilin ke-" + n + " dengan panjang awal: " + meltCandle);
	}

}
