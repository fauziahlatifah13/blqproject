package number11;

import java.util.Scanner;

public class NumberEleven {
	
	// Bintang-bintang
	
	public static void main(String[] args) {
		// INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan kata: ");
		String kata = input.next();
		String tempKata = kata.toLowerCase();

		
		// PROSES
		char[] huruf = tempKata.toCharArray();
		String star = "";
		for(int i=0; i<huruf.length; i++) {
			if(huruf[i]=='a' || huruf[i]=='i' || huruf[i]=='u' || huruf[i]=='e' || huruf[i]=='o') {
				star += "*";
			}
		}
		
		
		// OUTPUT
		char[] chHuruf = kata.toCharArray();
		for(int i=0; i<chHuruf.length; i++) {
			System.out.println(star + chHuruf[chHuruf.length-1-i] + star);			
		}
	}
}
