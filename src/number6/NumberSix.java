package number6;

import java.util.Scanner;

public class NumberSix {

	// Fungsi Palindrome
	
	public static void main(String[] args) {
		//INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan kata: ");
		String word = input.next();
		
		
		//PROSES
		String output = "Not Palindrome";
		String newWord = "";
		char[] chWord = word.toCharArray();
		for(int i=0; i<chWord.length; i++) {
			newWord += chWord[chWord.length-i-1];
		}
		
		if(newWord.equalsIgnoreCase(word)) {
			output = "Palindrome";
		}
		
		//OUTPUT
		System.out.println(output);
	}
}
