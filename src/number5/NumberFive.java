package number5;

import java.util.Scanner;

public class NumberFive {
	
	//Fungsi menampilkan n bilangan fibonacci pertama

	public static void main(String[] args) {
		// INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		
		
		// PROSES
		int[] fibonacci = new int[n]; 
		fibonacci[0] = 1; //bilangan fibonacci pertama
		fibonacci[1] = 1; //bilangan fibonacci kedua
		
		for(int i=2; i<n; i++) {
			fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
		}
		
		String output = "";
		for(int i=0; i<n; i++) {
			if(i+1==n) {
				output += fibonacci[i];
			}else {
				output += fibonacci[i] + ", ";
			}
		}
		
		
		// OUTPUT
		System.out.println(n + " bilangan prima pertama adalah " + output);
	}

}
