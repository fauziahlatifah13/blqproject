package number19;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NumberNineteen {

	//Fungsi cek pangram
	
	public static void main(String[] args) {
		//INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan kalimat: ");
		String kalimat = input.nextLine();
		
		
		//PROSES
		String result = "Not Pangram";
	    
	    List<Integer> alpha = new ArrayList<>();
	    int a = 97;
	    while(a<=122){
	        alpha.add(a);
	        a++;
	    }
	    
	    char[] kata = kalimat.toLowerCase().toCharArray();
	    for(int i=0; i<kata.length; i++){
	        for(int j=0; j<alpha.size(); j++){
	            if(alpha.get(j) == kata[i]){
	                alpha.remove(j);
	                break;
	            }
	        }
	        if(alpha.size()==0){
	            result = "Pangram";
	        }
	    }
	    
	    
		// OUTPUT
		System.out.println(result);
	}
}
