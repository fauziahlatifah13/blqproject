package number1;

import java.util.Scanner;

public class NumberOne {

	// Jumlah maksimal uang yang dipakai membeli barang
	
	public static void main(String[] args) {
		//INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nominal uang: ");
		int uang = input.nextInt();
		
		
		//PROSES
		int[] kacaMata = {500, 600, 700, 800};
		int[] baju = {200, 400, 350};
		int[] sepatu = {400, 350, 200, 300};
		int[] buku = {100, 50, 150};
		int total = 0;
		int[] index = new int[4];
		
		for(int i=0; i<kacaMata.length; i++) {
			for(int j=0; j<baju.length; j++) {
				for(int k=0; k<sepatu.length; k++) {
					for(int l=0; l<buku.length; l++) {
						int temp = kacaMata[i] + baju[j] + sepatu[k] + buku[l];
						if(temp>total && temp<=uang) {
							total = temp;
							index[0] = i;
							index[1] = j;
							index[2] = k;
							index[3] = l;
						}
					}
				}
			}
		}
		int sum = kacaMata[index[0]] + baju[index[1]] + sepatu[index[2]] + buku[index[3]];
		
		
		//OUTPUT
		System.out.println("Jumlah uang yang dipakai: " + sum);
		System.out.println("Jumlah item yang bisa dibeli: ");
		System.out.print("Kaca mata " + kacaMata[index[0]] + ", ");
		System.out.print("Baju " + baju[index[1]] + ", ");
		System.out.print("Sepatu " + sepatu[index[2]] + ", ");
		System.out.print("Buku " + buku[index[3]]);
	}
}
