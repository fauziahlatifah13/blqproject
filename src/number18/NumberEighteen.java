package number18;

import java.util.Scanner;

public class NumberEighteen {

	// Menghitung cc air yang diminum
	
	public static void main(String[] args) {
		//INPUT
		int[][] jamKalori = {{9, 13, 15, 17}, {30, 20, 50, 80}};
		int jamOlahraga = 18;
		
		
		//PROSES
		int ccWater = 0;
		for(int i=0; i<jamKalori[0].length; i++) {
			double minOlahraga = 0.1 * jamKalori[1][i] * (jamOlahraga-jamKalori[0][i]);
			
			if(minOlahraga>=30) {
				ccWater += 100 * (minOlahraga/30);
			}
		}
		ccWater += 500;
		
		
		//OUTPUT
		System.out.println("Banyak air yang diminum Donna: " + ccWater + " cc");
	}

}
