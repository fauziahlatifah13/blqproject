package number3;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class NumberThree {

	//Kalkulasi tarif parkir
	
	public static void main(String[] args) {
		// INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan tanggal masuk parkir (format dd MM yyyy | HH:mm:ss): ");
		String tanggalMasuk = input.nextLine();
		
		System.out.print("Masukkan tanggal keluar parkir(format dd MM yyyy | HH:mm:ss): ");
		String tanggalKeluar = input.nextLine();
		
		
		// PROSES
		double tarif = 0;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MM yyyy | HH:mm:ss");
		LocalDateTime startDate = LocalDateTime.parse(tanggalMasuk, formatter);
		LocalDateTime endDate = LocalDateTime.parse(tanggalKeluar, formatter);

		long durasiParkir = Duration.between(startDate, endDate).toHours();

		if (durasiParkir <= 8) {
			tarif = durasiParkir * 1000;
		} else if (8 < durasiParkir && durasiParkir <= 24) {
			tarif = 8000;
		} else {
			tarif = 15000 + ((durasiParkir - 24) * 1000);
		}
		
		
		// OUTPUT
		System.out.println("Tarif parkir: " + tarif);
	}

}
