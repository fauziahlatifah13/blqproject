package number8;

public class NumberEight {

	// Nilai minimal dan maksimal penjumlahan 4 komponen
	
	public static void main(String[] args) {
		//INPUT
		int[] num = {1, 2, 4, 7, 8, 6, 9};
		
		
		//PROSES
		int min = 0;
		int max = 0;
		
		for(int i=0; i<num.length-1; i++) {
			int temp = num[i];
			for(int j=i+1; j<num.length; j++) {
				if(num[j]<num[i]) {
					num[i] = num[j];
					num[j] = temp;
					temp = num[i];
				}
			}
		}
		min = num[0] + num[1] + num[2] + num[3];
		max = num[3] + num[4] + num[5] + num[6];
		
		
		//OUTPUT
		System.out.println("Nilai minimal: " + min);
		System.out.println("Nilai maksimal: " + max);
	}
}
