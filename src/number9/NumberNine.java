package number9;

import java.util.Scanner;

public class NumberNine {

	// Fungsi deret
	
	public static void main(String[] args) {
		// INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();


		// PROSES
		int[] num = new int[n];
		int angka = n;
		for(int i=0; i<n; i++) {
			num[i] = angka;
			angka += n;
		}

		String output = "";
		for(int i=0; i<n; i++) {
			output += num[i] + " ";
		}
		
		
		// OUTPUT
		System.out.println(output);

	}
}
