package number14;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NumberFourteen {

	// Fungsi geser deret
	
	public static void main(String[] args) {
		//INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		
		
		//PROSES
		int[] deret = {3, 9, 0, 7, 1, 2, 4};
		List<Integer> newDeret = new ArrayList<>();
		
		for(int i=n; i<deret.length; i++) {
			newDeret.add(deret[i]);
		}
		for(int i=0; i<n; i++) {
			newDeret.add(deret[i]);
		}
		
		
		//OUTPUT
		System.out.println(newDeret.toString());
	}

}
