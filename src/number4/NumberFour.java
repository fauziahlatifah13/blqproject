package number4;

import java.util.Scanner;

public class NumberFour {

	//Fungsi menampilkan n bilangan prima pertama
	
	public static void main(String[] args) {
		// INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		
		
		// PROSES
		int[] prime = new int[n]; 
		prime[0] = 2; //bilangan prima pertama
		int num = 3;
		
		for(int i=1; i<n; i++) {
			int j=2;
			while(j<num) {
				if(num%j==0) {
					num += 1;
					j=2;
					continue;
				}
				j++;
			}
			prime[i] = num;
			num += 1;
			j=2;
		}
		
		String output = "";
		for(int i=0; i<n; i++) {
			if(i+1==n) {
				output += prime[i];
			}else {
				output += prime[i] + ", ";
			}
		}
						
		
		// OUTPUT
		System.out.println(n + " bilangan prima pertama adalah " + output);
	}

}
