package number13;

import java.util.Scanner;

public class NumberThirteen {

	// Sudut terkecil 2 jarum jam
	
	public static void main(String[] args) {
		// INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Jam: ");
		String jam = input.nextLine();
		
		
		// PROSES
		String[] waktu = jam.split(":");
		int hour = Integer.parseInt(waktu[0]);
		int minute = Integer.parseInt(waktu[1]);
		int a = Math.abs(hour - (minute/5))*30;
		int b = minute/2;
		
		int tetha = a - b;
		if(tetha>180) {
			tetha = 360 - tetha;
		}
		
		
		// OUTPUT
		System.out.println(tetha + " derajat");
	}

}
