package number17;

import java.util.Scanner;

public class NumberSeventeen {

	// Hattori
	
	public static void main(String[] args) {
		// INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan inputan: ");
		String hattori = input.nextLine();

		
		// Proses
		String[] perjalanan = hattori.toUpperCase().split(" ");
		int gunung = 0;
		int lembah = 0;
		int mdplGunung = 0;
		int mdplLembah = 0;
		
		for (int i = 0; i < perjalanan.length - 1; i++) {
			if (perjalanan[i].equals("N")) {
				mdplGunung += 1;
				mdplLembah -= 1;
			} else if (perjalanan[i].equals("T")) {
				mdplGunung -= 1;
				mdplLembah += 1;
			}
			
			if(mdplGunung==0) {
				gunung += 1;
			}
			if(mdplLembah==0) {
				lembah += 1;
			}
		}

		
		// Output
		System.out.println("Gunung yang dilewati : " + gunung);
		System.out.println("Lembah yang dilewati : " + lembah);
	}
}
