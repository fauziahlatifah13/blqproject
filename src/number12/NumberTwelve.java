package number12;

import java.util.Scanner;

public class NumberTwelve {

	// Fungsi Sort
	
	public static void main(String[] args) {
		//INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan deret angka: ");
		String deret = input.nextLine();
		
		
		//PROSES
		String output = "";
		String[] angka = deret.split(" ");
		int[] intAngka = new int[angka.length];
		
		for(int i=0; i<angka.length; i++) {
			intAngka[i] = Integer.parseInt(angka[i]);
		}
		
		for(int i=0; i<intAngka.length-1; i++) {
			int temp = intAngka[i];
			for(int j=i+1; j<intAngka.length; j++) {
				if(intAngka[j]<intAngka[i]){
					intAngka[i] = intAngka[j];
					intAngka[j] = temp;
					temp = intAngka[i];
				}
			}
		}
		
		for(int a: intAngka) {
			output += a;
		}
		
		
		//OUTPUT
		System.out.println(output);
	}
}
