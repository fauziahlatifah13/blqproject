package number10;

import java.util.Scanner;

public class NumberTen {

	// bintang-bintang

	public static void main(String[] args) {
		// INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan kalimat: ");
		String kalimat = input.nextLine();


		// PROSES
		String output = "";
		String[] arrKata = kalimat.split(" ");

		for (int i = 0; i < arrKata.length; i++) {
			char[] charKata = arrKata[i].toCharArray();
			output += charKata[0] + "***" + charKata[charKata.length - 1] + " ";
		}

		
		// OUTPUT
		System.out.println(output);

	}

}
