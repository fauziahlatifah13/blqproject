package number21;

import java.util.Scanner;

public class NumberTwentyOne {
	
	//Beware the Manhole
	
	public static void main(String[] args) {
		//INPUT
		Scanner scanner = new Scanner(System.in);
		System.out.println("Lintasan berjumlah 9 dan berbentuk '_' atau 'O', diakhiri kata 'FINISH'");
		System.out.print("Masukkan lintasan: ");
		String lintasan = scanner.nextLine();

		
		//PROSES
		char[] chLintasan = lintasan.replaceAll(" ", "").toCharArray();
        int stamina = 0;
        int distance = 0;
        String output = "";
		
        for (int i = 1; i < chLintasan.length; i++) {
            if (chLintasan[0] == 'O') {
            	output = "FAILED";
            	break;
            }
            if (chLintasan[i] == '_' && chLintasan.length - i == 9 && lintasan.endsWith("_ _ _ Finish") && stamina >= 2) {
                output += "J ";
                break;
            } else if (chLintasan[i] == 'O') {
                if (stamina >= 2) {
                    stamina -= 2;
                    distance += 3;
                    output += "J ";
                } else {
                	output = "FAILED";
                }
            } else if (chLintasan[i] == '_') {
                stamina += 1;
                distance += 1;
                output += "W ";
            } else if (chLintasan[i] == 'F') {
                break;
            }
        }
		
        
        //OUTPUT
        System.out.println(output);
	}
}
