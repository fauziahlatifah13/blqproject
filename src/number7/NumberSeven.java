package number7;

public class NumberSeven {

	// Mencari mean, median, modus

	public static void main(String[] args) {
		// INPUT
		int[] num = {8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3};
		
		// PROSES
		int mean = 0;
		int median = 0;
		int mode = 0;
		
		//mean
		int total = 0;
		for(int i=0; i<num.length; i++) {
			total += num[i];
		}
		mean = total/num.length;
		
		//median
		for(int i=0; i<num.length-1; i++) {
			int temp = num[i];
			for(int j=i+1; j<num.length; j++) {
				if(num[j]<num[i]) {
					num[i] = num[j];
					num[j] = temp;
					temp = num[i];
				}
			}
		}
		System.out.println((num.length)/2);
		if((num.length)%2 == 0) {
			median = (num[((num.length)/2)-1] + num[(num.length)/2])/2;
		}else {
			median = num[(num.length)/2];
		}

		
		//modus
		int countNum = 0;
		int tempCount = 1;
		for(int i=1; i<num.length; i++) {
			if(num[i]==num[i-1]) {
				tempCount += 1;
			}else {
				if(tempCount>countNum) {
					countNum = tempCount;
					mode = num[i-1];
				}
				tempCount = 1;
			}
		}
		
		// OUTPUT
		System.out.println("Mean: " + mean);
		System.out.println("Median: " + median);
		System.out.println("Modus: " + mode);
	}
}
