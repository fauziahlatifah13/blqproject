package number15;

import java.util.Scanner;

public class NumberFifteen {

	//Fungsi time conversion
	
	public static void main(String[] args) {
		//INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan waktu: ");
		String waktu = input.nextLine();
		
		
		//PROSES
		String result = "";
		String amPM = waktu.substring(8,10);
		if(amPM.equalsIgnoreCase("AM")) {
			result = waktu.substring(0,8);
		}else {
			int jam = Integer.parseInt(waktu.substring(0,2));
			result = (jam + 12) + waktu.substring(2,8);
		}
		
		
		//OUTPUT
		System.out.println(result);
	}
}
