package number16;

import java.util.Scanner;

public class NumberSixteen {

	// Fungsi hitung uang ptpt
	
	public static void main(String[] args) {
		//INPUT
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan banyak makanan yang dipesan: ");
		int banyakPesanan = input.nextInt();
		
		int[] harga = new int[banyakPesanan];
		System.out.println();
		System.out.println("Masukkan harga-harga makanan!");
		for(int i=0; i<banyakPesanan; i++) {
			System.out.print("Harga makanan ke-" + (i+1) + " : ");
			harga[i] = input.nextInt();
		}
		
		System.out.print("Makanan ke berapa yang mengandung ikan? ");
		int ikan = input.nextInt();
		
		
		//PROSES
		double ptpt1 = 0;
		double ptpt2 = 0;
		for(int i=0; i<banyakPesanan; i++) {
			if(i==ikan-1) {
				ptpt1 += harga[i]/3;
			}else {
				ptpt1 += harga[i]/4;
				ptpt2 += harga[i]/4;
			}
		}
		
		ptpt1 = ptpt1*(1 + 0.1 + 0.5); //service + pajak
		ptpt2 = ptpt2*(1 + 0.1 + 0.5);
		
		
		//OUTPUT
		System.out.println();
		System.out.println("Total uang yang harus dibayar masing-masing: ");
		for(int i=0; i<3; i++) {
			System.out.print("Rp" + (int)ptpt1 + ", ");
		}
		System.out.println("Rp" + (int)ptpt2);
	}
}
