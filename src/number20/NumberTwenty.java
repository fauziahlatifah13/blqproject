package number20;

import java.util.Scanner;

public class NumberTwenty {

	//Rock Scissors Paper
	
	public static void main(String[] args) {
		//INPUT
		int jarakAwal = 2;
		String[] a = {"G", "G", "G"};
		String[] b = {"K", "K", "B"};		
		
		//PROSES
		String[] result = new String[3];
		String output = "";
		for (int i = 0; i < a.length; i++) {
            if (a[i].equals(b[i])) {
            	result[i] = "Draw";
            } else if ((a[i].equals("G") && b[i].equals("K")) ||
                    (a[i].equals("K") && b[i].equals("B")) ||
                    (a[i].equals("B") && b[i].equals("G"))) {
            	result[i] = "Win";
            } else {
            	result[i] = "Lose";
            }
        }
		
		
		int posisiA = 0;
        int posisiB = jarakAwal;
        int lastRound = -1;

        for (int i = 0; i < result.length; i++) {
            if (posisiA == posisiB) {
                lastRound = i - 1;
                break;
            } else if (result[i].equals("Win")) {
            	posisiA += 2;
            	posisiB += 1;
            } else if (result[i].equals("Lose")) {
            	posisiA -= 1;
            	posisiB -= 2;
            }
        }

        if (lastRound >= 0 && result[lastRound].equals("Win")) {
            output = "A";
        } else if (lastRound >= 0 && result[lastRound].equals("Lose")) {
            output = "B";
        }else {
        	output = "Draw";
        }
		
        
		//OUTPUT
		System.out.println("Pemenangnya adalah: " + output);
	}

}
